import { Route, Switch, Redirect } from "react-router-dom";
import Login from "./components/Users/Login";
import SignUp from "./components/Users/SignUp";
import { RequireAuth } from "./components/Authed/RequireAuth";
import Profile from "./components/Students/Profile";
import VacancyList from "./components/Company/Vacancies";
import MyStudents from "./components/University/MyStudents";
import Students from "./components/Company/Students";
import editProfile from "./components/Students/EditProfile";
import "./sass/styles.scss";

export default function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/login" component={Login} />
        <Route path="/signup" component={SignUp} />
        <RequireAuth>
          <Route path="/mystudents" component={MyStudents} />
          <Route path="/students" component={Students} />
          <Route path="/myprofile" component={Profile} />
          <Route path="/editProfile" component={editProfile} />
          <Route path="/vacancies" component={VacancyList} />
        </RequireAuth>
        <Redirect to="/login" />
      </Switch>
    </div>
  );
}
