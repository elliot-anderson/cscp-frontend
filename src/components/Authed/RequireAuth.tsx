import { Redirect } from "react-router-dom";
import { Route } from "react-router-dom";
export function RequireAuth({ children, ...rest }: any) {
  return (
    <Route
      {...rest}
      render={() => {
        return localStorage.token ? children : <Redirect to="/login" />;
      }}
    />
  );
}
