export default function LoginError(props: any) {
  return (
    <div className="flash">
      <div className="notification is-danger is-light">
        <p>{props.message}</p>
      </div>
    </div>
  );
}
