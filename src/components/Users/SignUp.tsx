import { Link } from "react-router-dom";
import { useForm } from "react-hook-form";
import axios from "axios";

export default function SignUp() {
  const { register, handleSubmit } = useForm();
  const headers = {
    "Content-Type": "Application/json",
  };
  const onSubmit = (signUpData: any) => {
    axios
      .post("http://localhost:8080/users/signup", JSON.stringify(signUpData), {
        headers: headers,
      })
      .then((data) => {
        // else{
        //     this.props.history.push('/login')
        // }
        console.log(data);
      });
  };

  return (
    <>
      <div id="login">
        <div className="login-card">
          <div className="card-title">
            <h1 className="is-size-4 has-text-weight-bold">Sign Up to CSCP</h1>
          </div>
          <div className="login-content">
            <form onSubmit={handleSubmit(onSubmit)}>
              <select
                className="roles"
                {...register("role")}
                required
                defaultChecked
              >
                <option value="">Choose User type</option>
                <option value="student">Student</option>
                <option value="university">University Member</option>
                <option value="company">Company member</option>
              </select>
              <input
                className="fname"
                type="text"
                required
                {...register("fname")}
                placeholder="First Name"
              />
              <input
                className="mname"
                type="text"
                {...register("mname")}
                placeholder="Middle Name"
              />
              <input
                className="lname"
                type="text"
                required
                {...register("lname")}
                placeholder="Last Name"
              />
              <input
                className="email"
                type="text"
                required
                {...register("email")}
                placeholder="Email"
              />
              <input
                className="password"
                type="password"
                required
                {...register("password")}
                placeholder="Password"
              />
              <button type="submit" className="btn btn-primary btn btn-link">
                Sign up
              </button>

              <div className="options">
                <p>
                  Have an account?{" "}
                  <Link to={{ pathname: "/login" }}>Log in</Link>
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
