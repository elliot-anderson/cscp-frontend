import { Link, useHistory } from "react-router-dom";
import axios from "axios";
import { useForm } from "react-hook-form";

export default function Login() {
  const { register, handleSubmit } = useForm();
  const history = useHistory();
  const headers = {
    "Content-Type": "Application/json",
  };

  const handleLoginForm = (loginData: any) => {
    axios
      .post("http://localhost:8080/users/login", JSON.stringify(loginData), {
        headers: headers,
      })
      .then((data) => {
        localStorage.setItem("token", `Bearer ${data.data.token}`);
        localStorage.setItem("role", data.data.user.role);
        console.log(localStorage.role);
        if (data.data.user.role === "student") history.push("/myprofile");
        else if (data.data.user.role === "university")
          history.push("/mystudents");
        else if (data.data.user.role === "company") history.push("/students");
      });
  };

  return (
    <>
      <div id="login">
        <div className="login-card">
          <div className="card-title">
            <h1 className="is-size-4 has-text-weight-bold">Login to CSCP</h1>
          </div>
          <div className="login-content">
            <form onSubmit={handleSubmit(handleLoginForm)}>
              <input
                className="email"
                {...register("email")}
                type="text"
                required
                placeholder="Enter your email"
              />
              <input
                className="password"
                type="password"
                required
                {...register("password")}
                placeholder="Enter your password"
              />
              <button type="submit" className="btn btn-primary">
                Login
              </button>

              <div className="options">
                <p>
                  Need an account?{" "}
                  <Link to={{ pathname: "/signup" }}>Sign up</Link>
                </p>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
}
