import { Component } from "react";
import { Link } from "react-router-dom";

export default class NotFound extends Component {
  render() {
    return (
      <div>
        <section className="section">
          <div className="container box content">
            <div className="columns">
              <div className="column is-full has-background-white">
                <h1 className="has-text-centered is-size-3 has-text-weight-bold">
                  What you are looking for not in us!
                </h1>
                <div>
                  <p className="center">
                    <Link
                      className="has-text-primary is-size-2 has-text-weight-bold"
                      to="/login"
                    >
                      Back to Login page
                    </Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
