import React, { Component } from "react";
import NavBar from "../NavBar/NavBar";
import axios from "axios";
import ContentEditable, { ContentEditableEvent } from "react-contenteditable";

interface IProps {
  history: any;
  user: any;
}
interface IProfile {
  student_id: number | any;
  role: string | any;
  fname: string | any;
  mname: string | any;
  lname: string | any;
  email: string | any;
  phone_number: string | any;
  employment_status: string | any;
  job_opportunities: string | any;
  job_title: string | any;
  objectives: string | any;
  institute_names_array: string[] | any;
  institute_degree_names_array: string[] | any;
  institute_locations_array: string[] | any;
  field_of_studies_array: string[] | any;
  study_intervals_array: string[] | any;
  gpa_score: string[] | any;
  company_names_array: string[] | any;
  company_locations_array: string[] | any;
  job_titles_array: string[] | any;
  job_descriptions_array: string[] | any;
  work_intervals_array: string[] | any;
  project_titles_array: string[] | any;
  project_descriptions_array: string[] | any;
  skills_array: string[] | any;
  skill_levels_array: number[] | any;
  area_of_interests: string | any;
}
export default class MyProfile extends Component<IProps> {
  state = {
    student_id: null,
    role: "",
    fname: "",
    mname: "",
    lname: "",
    email: "",
    phone_number: "",
    employment_status: "",
    job_opportunities: "",
    job_title: "",
    objectives: "",
    institute_names_array: [],
    institute_degree_names_array: [],
    institute_locations_array: [],
    field_of_studies_array: [],
    study_intervals_array: [],
    gpa_score: [],
    company_names_array: [],
    company_locations_array: [],
    job_titles_array: [],
    job_descriptions_array: [],
    work_intervals_array: [],
    project_titles_array: [],
    project_descriptions_array: [],
    skills_array: [],
    skill_levels_array: [],
    area_of_interests: "",
  };
  stateCopy: IProfile = Object.assign({}, this.state);
  componentDidMount() {
    if (!localStorage.token) {
      this.props.history.push("/login");
    } else {
      this.getProfile();
    }
  }

  getProfile = async () => {
    if (localStorage.token) {
      await axios
        .get("http://localhost:8080/users/myprofile", {
          headers: {
            Authorization: localStorage.token,
          },
        })
        .then((data: any) => {
          this.setState({
            student_id: data.data[0].student_id,
            role: data.data[0].role,
            fname: data.data[0].fname,
            mname: data.data[0].mname,
            lname: data.data[0].lname,
            email: data.data[0].email,
            phone_number: data.data[0].phone_number,
            employment_status: data.data[0].employment_status,
            job_opportunities: data.data[0].job_opportunities,
            job_title: data.data[0].job_title,
            objectives: data.data[0].objectives,
            institute_names_array: data.data[0].institute_names_array,
            institute_degree_names_array:
              data.data[0].institute_degree_names_array,
            institute_locations_array: data.data[0].institute_locations_array,
            field_of_studies_array: data.data[0].field_of_studies_array,
            study_intervals_array: data.data[0].study_intervals_array,
            gpa_score: data.data[0].gpa_score,
            company_names_array: data.data[0].company_names_array,
            company_locations_array: data.data[0].company_locations_array,
            job_titles_array: data.data[0].job_titles_array,
            job_descriptions_array: data.data[0].job_descriptions_array,
            work_intervals_array: data.data[0].work_intervals_array,
            project_titles_array: data.data[0].project_titles_array,
            project_descriptions_array: data.data[0].project_descriptions_array,
            skills_array: data.data[0].skills_array,
            skill_levels_array: data.data[0].skill_levels_array,
            area_of_interests: data.data[0].area_of_interests,
          });
        });
    }
  };
  onSubmit = async () => {
    await axios
      .put(
        "http://localhost:8080/users/myprofile",

        this.state,
        {
          headers: {
            Authorization: localStorage.token,
          },
        }
      )
      .then(() => this.props.history.push("/myprofile"));
  };
  render() {
    return (
      <div>
        <NavBar history={this.props.history} user={this.state} />
        <div className="container_x">
          <div className="header">
            <div className="full-name">
              <span className="first-name">{this.state.fname}</span>
              <span className="last-name">{this.state.lname}</span>
            </div>
            <div className="contact-info">
              <span className="email-val">
                <a href={`mailto:${this.state.email}`}>{this.state.email}</a>
              </span>
              <span className="separator"></span>
              <ContentEditable
                onChange={(event: ContentEditableEvent) => {
                  this.setState({ phone_number: event.target.value });
                }}
                html={this.state.phone_number}
                disabled={false}
                className="phone-val"
              />
            </div>

            <div className="about">
              <ContentEditable
                onChange={(e: ContentEditableEvent) =>
                  this.setState({ job_title: e.target.value })
                }
                html={this.state.job_title}
                disabled={false}
                className="position"
              />
              <ContentEditable
                onChange={(e: ContentEditableEvent) =>
                  this.setState({ objectives: e.target.value })
                }
                className="desc"
                html={this.state.objectives}
                disabled={false}
              />
            </div>
          </div>
          <div className="details">
            <div className="section">
              <div className="section__title">Experience</div>
              <div className="section__list">
                {this.state.company_names_array?.map((_: any, i: number) => {
                  return (
                    <div key={i} className="section__list-item">
                      <div className="left">
                        <div className="name">
                          <ContentEditable
                            onChange={(e: ContentEditableEvent) => {
                              this.stateCopy = Object.assign({}, this.state);
                              this.stateCopy.company_names_array = this.stateCopy.company_names_array.slice();
                              this.stateCopy.company_names_array[i] =
                                e.target.value;
                              this.setState({
                                company_names_array: this.stateCopy
                                  .company_names_array,
                              });
                            }}
                            html={this.state.company_names_array[i]}
                            disabled={false}
                          />
                          <div style={{ color: "purple" }}>
                            <ContentEditable
                              onChange={(e: ContentEditableEvent) => {
                                this.stateCopy = Object.assign({}, this.state);
                                this.stateCopy.job_titles_array = this.stateCopy.job_titles_array.slice();
                                this.stateCopy.job_titles_array[i] =
                                  e.target.value;
                                this.setState({
                                  job_titles_array: this.stateCopy
                                    .job_titles_array,
                                });
                              }}
                              html={this.state.job_titles_array[i]}
                              disabled={false}
                            />
                          </div>
                        </div>
                        <div className="addr" style={{ color: "green" }}>
                          <ContentEditable
                            disabled={false}
                            onChange={(e: ContentEditableEvent) => {
                              this.stateCopy = Object.assign({}, this.state);
                              this.stateCopy.company_locations_array = this.stateCopy.company_locations_array.slice();
                              this.stateCopy.company_locations_array[i] =
                                e.target.value;
                              this.setState({
                                company_locations_array: this.stateCopy
                                  .company_locations_array,
                              });
                            }}
                            html={this.state.company_locations_array[i]}
                          />{" "}
                          |{" "}
                          <ContentEditable
                            onChange={(e: ContentEditableEvent) => {
                              this.stateCopy = Object.assign({}, this.state);
                              this.stateCopy.work_intervals_array = this.stateCopy.work_intervals_array.slice();
                              this.stateCopy.work_intervals_array[i] =
                                e.target.value;
                              this.setState({
                                work_intervals_array: this.stateCopy
                                  .work_intervals_array,
                              });
                            }}
                            disabled={false}
                            html={this.state.work_intervals_array[i]}
                          />
                        </div>
                      </div>
                      <div>
                        <div className="desc">
                          <ContentEditable
                            onChange={(e: ContentEditableEvent) => {
                              this.stateCopy = Object.assign({}, this.state);
                              this.stateCopy.job_descriptions_array = this.stateCopy.job_descriptions_array.slice();
                              this.stateCopy.job_descriptions_array[i] =
                                e.target.value;
                              this.setState({
                                job_descriptions_array: this.stateCopy
                                  .job_descriptions_array,
                              });
                            }}
                            html={this.state.job_descriptions_array[i]}
                            disabled={false}
                          />
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="section">
              <div className="section__title">Education</div>
              <div className="section__list">
                {this.state.institute_names_array?.map((_: any, i: number) => {
                  return (
                    <div key={i} className="section__list-item">
                      <div className="left">
                        <div className="name">
                          <ContentEditable
                            onChange={(e: ContentEditableEvent) => {
                              this.stateCopy = Object.assign({}, this.state);
                              this.stateCopy.institute_names_array = this.stateCopy.institute_names_array.slice();
                              this.stateCopy.institute_names_array[i] =
                                e.target.value;
                              this.setState({
                                institute_names_array: this.stateCopy
                                  .institute_names_array,
                              });
                            }}
                            disabled={false}
                            html={this.state.institute_names_array[i]}
                          />
                          <div style={{ color: "purple" }}>
                            <ContentEditable
                              onChange={(e: ContentEditableEvent) => {
                                this.stateCopy = Object.assign({}, this.state);
                                this.stateCopy.institute_degree_names_array = this.stateCopy.institute_degree_names_array.slice();
                                this.stateCopy.institute_degree_names_array[i] =
                                  e.target.value;
                                this.setState({
                                  institute_degree_names_array: this.stateCopy
                                    .institute_degree_names_array,
                                });
                              }}
                              html={this.state.institute_degree_names_array[i]}
                              disabled={false}
                            />{" "}
                            |{" "}
                            <ContentEditable
                              onChange={(e: ContentEditableEvent) => {
                                this.stateCopy = Object.assign({}, this.state);
                                this.stateCopy.field_of_studies_array = this.stateCopy.field_of_studies_array.slice();
                                this.stateCopy.field_of_studies_array[i] =
                                  e.target.value;
                                this.setState({
                                  field_of_studies_array: this.stateCopy
                                    .field_of_studies_array,
                                });
                              }}
                              disabled={false}
                              html={this.state.field_of_studies_array[i]}
                            />
                          </div>
                        </div>
                        <div className="addr" style={{ color: "green" }}>
                          <ContentEditable
                            onChange={(e: ContentEditableEvent) => {
                              this.stateCopy = Object.assign({}, this.state);
                              this.stateCopy.institute_locations_array = this.stateCopy.institute_locations_array.slice();
                              this.stateCopy.institute_locations_array[i] =
                                e.target.value;
                              this.setState({
                                institute_locations_array: this.stateCopy
                                  .institute_locations_array,
                              });
                            }}
                            disabled={false}
                            html={this.state.institute_locations_array[i]}
                          />{" "}
                          |{" "}
                          <ContentEditable
                            onChange={(e: ContentEditableEvent) => {
                              this.stateCopy = Object.assign({}, this.state);
                              this.stateCopy.study_intervals_array = this.stateCopy.study_intervals_array.slice();
                              this.stateCopy.study_intervals_array[i] =
                                e.target.value;
                              this.setState({
                                study_intervals_array: this.stateCopy
                                  .study_intervals_array,
                              });
                            }}
                            html={this.state.study_intervals_array[i]}
                            disabled={false}
                          />
                        </div>
                        <div className="desc">
                          GPA Score:{" "}
                          <ContentEditable
                            disabled={false}
                            onChange={(e: ContentEditableEvent) => {
                              this.stateCopy = Object.assign({}, this.state);
                              this.stateCopy.gpa_score = this.stateCopy.gpa_score.slice();
                              this.stateCopy.gpa_score[i] = e.target.value;
                              this.setState({
                                gpa_score: this.stateCopy.gpa_score,
                              });
                            }}
                            html={this.state.gpa_score[i]}
                          />
                        </div>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="section">
              <div className="section__title">Projects</div>
              <div className="section__list">
                {this.state.project_titles_array?.map((_: any, i: number) => {
                  return (
                    <div className="section__list-item" key={i}>
                      <div className="name">
                        {" "}
                        <ContentEditable
                          disabled={false}
                          onChange={(e: ContentEditableEvent) => {
                            this.stateCopy = Object.assign({}, this.state);
                            this.stateCopy.project_titles_array = this.stateCopy.project_titles_array.slice();
                            this.stateCopy.project_titles_array[i] =
                              e.target.value;
                            this.setState({
                              project_titles_array: this.stateCopy
                                .project_titles_array,
                            });
                          }}
                          html={this.state.project_titles_array[i]}
                        />
                      </div>
                      <div className="text">
                        <ContentEditable
                          disabled={false}
                          onChange={(e: ContentEditableEvent) => {
                            this.stateCopy = Object.assign({}, this.state);
                            this.stateCopy.project_descriptions_array = this.stateCopy.project_descriptions_array.slice();
                            this.stateCopy.project_descriptions_array[i] =
                              e.target.value;
                            this.setState({
                              project_descriptions_array: this.stateCopy
                                .project_descriptions_array,
                            });
                          }}
                          html={this.state.project_descriptions_array[i]}
                        />
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="section">
              <div className="section__title">Skills</div>
              <div className="skills">
                {this.state.skill_levels_array?.map((_: any, i: number) => {
                  return (
                    <div key={i} className="skills__item">
                      <div className="left">
                        <div className="name">
                          {" "}
                          <ContentEditable
                            disabled={false}
                            onChange={(e: ContentEditableEvent) => {
                              this.stateCopy = Object.assign({}, this.state);
                              this.stateCopy.skills_array = this.stateCopy.skills_array.slice();
                              this.stateCopy.skills_array[i] = e.target.value;
                              this.setState({
                                skills_array: this.stateCopy.skills_array,
                              });
                            }}
                            html={this.state.skills_array[i]}
                          />
                        </div>
                      </div>
                      <div className="right">
                        <ContentEditable
                          disabled={false}
                          onChange={(e: ContentEditableEvent) => {
                            this.stateCopy = Object.assign({}, this.state);
                            this.stateCopy.skill_levels_array = this.stateCopy.skill_levels_array.slice();
                            this.stateCopy.skill_levels_array[i] = Number(
                              e.target.value
                            );
                            this.setState({
                              skill_levels_array: this.stateCopy
                                .skill_levels_array,
                            });
                          }}
                          html={String(this.state.skill_levels_array[i])}
                        />
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className="section">
              <div className="section__title">Interests</div>
              <div className="section__list">
                <div className="section__list-item">
                  <ContentEditable
                    onChange={(e: ContentEditableEvent) =>
                      this.setState({
                        area_of_interests: e.target.value,
                      })
                    }
                    html={this.state.area_of_interests}
                    disabled={false}
                  />

                  <div style={{ textAlign: "right" }}>
                    <button
                      className="button"
                      type="submit"
                      onClick={this.onSubmit.bind(this)}
                    >
                      Save Changes
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
