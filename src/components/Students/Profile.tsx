import React, { useState, useEffect } from "react";
import NavBar from "../NavBar/NavBar";
import axios from "axios";
import { useHistory } from "react-router-dom";
interface IProps {
  history: any;
  user: any;
}

export default function MyProfile<IProps>() {
  const history = useHistory();
  const [state, setState] = useState({
    role: "",
    fname: "",
    mname: "",
    lname: "",
    email: "",
    phone_number: "",
    employment_status: "",
    job_opportunities: "",
    job_title: "",
    objectives: "",
    institute_names_array: [],
    institute_degree_names_array: [],
    institute_locations_array: [],
    field_of_studies_array: [],
    study_intervals_array: [],
    gpa_score: [],
    company_names_array: [],
    company_locations_array: [],
    job_titles_array: [],
    job_descriptions_array: [],
    work_intervals_array: [],
    project_titles_array: [],
    project_descriptions_array: [],
    skills_array: [],
    skill_levels_array: [],
    area_of_interests: "",
  });

  useEffect(() => {
    if (!localStorage.token) {
      history.push("/login");
    } else {
      getProfile();
    }
  }, [localStorage.token]);

  const getProfile = async () => {
    if (localStorage.token) {
      await axios
        .get("http://localhost:8080/users/myprofile", {
          headers: {
            Authorization: localStorage.token,
          },
        })
        .then((data: any) => {
          setState({
            role: data.data[0].role,
            fname: data.data[0].fname,
            mname: data.data[0].mname,
            lname: data.data[0].lname,
            email: data.data[0].email,
            phone_number: data.data[0].phone_number,
            employment_status: data.data[0].employment_status,
            job_opportunities: data.data[0].job_opportunities,
            job_title: data.data[0].job_title,
            objectives: data.data[0].objectives,
            institute_names_array: data.data[0].institute_names_array,
            institute_degree_names_array:
              data.data[0].institute_degree_names_array,
            institute_locations_array: data.data[0].institute_locations_array,
            field_of_studies_array: data.data[0].field_of_studies_array,
            study_intervals_array: data.data[0].study_intervals_array,
            gpa_score: data.data[0].gpa_score,
            company_names_array: data.data[0].company_names_array,
            company_locations_array: data.data[0].company_locations_array,
            job_titles_array: data.data[0].job_titles_array,
            job_descriptions_array: data.data[0].job_descriptions_array,
            work_intervals_array: data.data[0].work_intervals_array,
            project_titles_array: data.data[0].project_titles_array,
            project_descriptions_array: data.data[0].project_descriptions_array,
            skills_array: data.data[0].skills_array,
            skill_levels_array: data.data[0].skill_levels_array,
            area_of_interests: data.data[0].area_of_interests,
          });
        });
    }
  };

  return (
    <div>
      <NavBar
        history={history}
        user={{
          fname: state.fname,
          lname: state.lname,
          email: state.email,
        }}
      />
      <div className="container_x">
        <div className="header">
          <div style={{ textAlign: "right" }}>
            <button
              className="button"
              onClick={() => history.push("/editProfile")}
            >
              Edit
            </button>
          </div>
          <div className="full-name">
            <span className="first-name">{state.fname}</span>
            <span className="last-name">{state.lname}</span>
          </div>
          <div className="contact-info">
            <span className="email-val">
              <a href={`mailto:${state.email}`}>{state.email}</a>
            </span>
            <span className="separator"></span>
            <span className="phone-val">{state.phone_number}</span>
          </div>

          <div className="about">
            <span className="position">{state.job_title}</span>
            <span className="desc">{state.objectives}</span>
          </div>
        </div>
        <div className="details">
          <div className="section">
            <div className="section__title">Experience</div>
            <div className="section__list">
              {state.company_names_array.map((_, i) => {
                return (
                  <div className="section__list-item" key={i}>
                    <div className="left">
                      <div className="name">
                        {state.company_names_array[i]}
                        <p style={{ color: "purple" }}>
                          {state.job_titles_array[i]}
                        </p>
                      </div>
                      <div className="addr" style={{ color: "green" }}>
                        {state.company_locations_array[i]} |{" "}
                        {state.work_intervals_array[i]}
                      </div>
                    </div>
                    <div>
                      <div className="desc">
                        {state.job_descriptions_array[i]}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="section">
            <div className="section__title">Education</div>
            <div className="section__list">
              {state.institute_names_array.map((_, i) => {
                return (
                  <div className="section__list-item" key={i}>
                    <div className="left">
                      <div className="name">
                        {state.institute_names_array[i]}
                        <p style={{ color: "purple" }}>
                          {state.institute_degree_names_array[i]} |{" "}
                          {state.field_of_studies_array[i]}
                        </p>
                      </div>
                      <div className="addr" style={{ color: "green" }}>
                        {state.institute_locations_array[i]} |{" "}
                        {state.study_intervals_array[i]}
                      </div>
                      <div className="desc">
                        GPA Score: {state.gpa_score[i]}
                      </div>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="section">
            <div className="section__title">Projects</div>
            <div className="section__list">
              {state.project_titles_array.map((_, i) => {
                return (
                  <div className="section__list-item" key={i}>
                    <div className="name">{state.project_titles_array[i]}</div>
                    <div className="text">
                      {state.project_descriptions_array[i]}
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="section">
            <div className="section__title">Skills</div>
            <div className="skills">
              {state.skill_levels_array.map((_, i) => {
                return (
                  <div key={i} className="skills__item">
                    <div className="left">
                      <div className="name">{state.skills_array[i]}</div>
                    </div>
                    <div className="right">
                      <input
                        id="ck1"
                        type="checkbox"
                        checked={state.skill_levels_array[i] >= 1}
                        readOnly
                      />

                      <label htmlFor="ck1"></label>
                      <input
                        id="ck2"
                        type="checkbox"
                        checked={state.skill_levels_array[i] >= 2}
                        readOnly
                      />

                      <label htmlFor="ck2"></label>
                      <input
                        id="ck3"
                        type="checkbox"
                        checked={state.skill_levels_array[i] >= 3}
                        readOnly
                      />

                      <label htmlFor="ck3"></label>
                      <input
                        id="ck4"
                        type="checkbox"
                        checked={state.skill_levels_array[i] >= 4}
                        readOnly
                      />
                      <label htmlFor="ck4"></label>
                      <input
                        id="ck5"
                        type="checkbox"
                        checked={state.skill_levels_array[i] === 5}
                        readOnly
                      />
                      <label htmlFor="ck5"></label>
                    </div>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="section">
            <div className="section__title">Interests</div>
            <div className="section__list">
              <div className="section__list-item">
                {state.area_of_interests}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
