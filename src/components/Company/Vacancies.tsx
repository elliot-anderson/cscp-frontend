import React, { Component } from "react";
import { Link } from "react-router-dom";
import NavBar from "../NavBar/NavBar";
import axios from "axios";
interface IProps {
  history: any;
}
interface IVacancy {
  title: string | any;
  fname: string | any;
  lname: string | any;
  body: string | any;
  name: string | any;
  start: Date | any;
  end: Date | any;
  num_of_positions: number | any;
  num_of_hired: number | any;
}
interface Vacancies {
  vacancies: IVacancy[];
}
export default class VacancyList extends Component<IProps> {
  state: Vacancies = {
    vacancies: [],
  };
  componentDidMount() {
    if (!localStorage.token) this.props.history("/login");
    else this.getVacancies();
  }
  getVacancies = async () => {
    await axios
      .get("http://localhost:8080/company/vacancies", {
        headers: { Authorization: localStorage.token },
      })
      .then((data) => {
        this.setState({ vacancies: data.data });
      });
  };

  renderVacancies() {
    return this.state.vacancies?.map((vacancy, id) => {
      return (
        <div>
          <div key={id}>
            {/* <Link
            className="is-size-4 has-text-weight-bold has-text-primary"
            to={{
              pathname: `/vacancy/${vacancy.id}`,
              state: { user: this.props.user },
            }}
          > */}
            {vacancy.title}
            <p className="is-size-7 has-text-weight-light">
              By {vacancy.fname} {vacancy.lname} from {vacancy.name}
              <p style={{ color: "blue", margin: 0, padding: 0 }}>
                Vacancy starts at: {vacancy.start.split("T")[0]}
              </p>
              <p style={{ color: "red", margin: 0, padding: 0 }}>
                Vacancy ends at:{" "}
                {vacancy.end ? vacancy.end.split("T")[0] : "Not specified"}
              </p>
              <p style={{ color: "green", margin: 0, padding: 0 }}>
                Number of positions:{" "}
                {vacancy.num_of_positions - vacancy.num_of_hired}
              </p>
            </p>
            <p>{vacancy.body}</p>
            <hr />
          </div>
        </div>
      );
    });
  }

  render() {
    return (
      <div>
        <div>
          <NavBar
            history={this.props.history}
            user={{
              fname: null,
              lname: null,
              email: null,
            }}
          />
        </div>
        <section className="section">
          <div className="container box content">
            <div className="columns">
              <div className="column is-full has-background-white">
                <h1 className="has-text-centered is-size-3 has-text-weight-bold">
                  Latest vacancies
                </h1>
                <div>{this.renderVacancies()}</div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
