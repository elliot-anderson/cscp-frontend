import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import NavBar from "../NavBar/NavBar";

interface IProfile {
  student_id: number | any;
  role: string | any;
  fname: string | any;
  mname: string | any;
  lname: string | any;
  email: string | any;
  phone_number: string | any;
  employment_status: string | any;
  job_opportunities: string | any;
  job_title: string | any;
  objectives: string | any;
  institute_names_array: string[] | any;
  institute_degree_names_array: string[] | any;
  institute_locations_array: string[] | any;
  field_of_studies_array: string[] | any;
  study_intervals_array: string[] | any;
  gpa_score: string[] | any;
  company_names_array: string[] | any;
  company_locations_array: string[] | any;
  job_titles_array: string[] | any;
  job_descriptions_array: string[] | any;
  work_intervals_array: string[] | any;
  project_titles_array: string[] | any;
  project_descriptions_array: string[] | any;
  skills_array: string[] | any;
  skill_levels_array: number[] | any;
  area_of_interests: string | any;
}
interface IProfiles {
  profiles: IProfile[];
}
interface IProps {
  history: any;
}
export default class MyStudents extends Component<IProps> {
  state: IProfiles = {
    profiles: [],
  };
  componentDidMount() {
    if (!localStorage.token) this.props.history("/login");
    else this.getProfiles();
  }
  getProfiles = async () => {
    await axios
      .get("http://localhost:8080/users/mystudents", {
        headers: {
          Authorization: localStorage.token,
        },
      })
      .then((data: any) => {
        this.setState({ profiles: data.data });
      });
  };
  renderMyStudents = () => {
    return this.state.profiles?.map((profile, i) => {
      return (
        <div key={i}>
          <Link
            className="is-size-4 has-text-weight-bold has-text-primary"
            to={{
              pathname: `/students/${i}`,
              state: { user: this.state.profiles[i] },
            }}
          >
            {profile.fname + " " + profile.lname}
          </Link>
          <p className="is-size-7 has-text-weight-light">{profile.email}</p>
          <p className="is-size-7 has-text-weight-light">
            Employment status: {profile.employment_status}
          </p>
          <p className="is-size-7 has-text-weight-light">
            Searching for job?: {profile.job_opportunities}
          </p>
          <p className="is-size-7 has-text-weight-light">
            GPA: {profile.gpa_score}
          </p>
          <hr />
        </div>
      );
    });
  };

  render() {
    return (
      <div>
        <div>
          <NavBar
            history={this.props.history}
            user={{
              fname: "",
              lname: "",
              email: "",
            }}
          />
        </div>
        <section className="section">
          <div className="container box content">
            <div className="columns">
              <div className="column is-full has-background-white">
                <h1 className="has-text-centered is-size-3 has-text-weight-bold">
                  My Students
                </h1>
                <div>{this.renderMyStudents()}</div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
