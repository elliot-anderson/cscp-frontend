import React from "react";
import { Link } from "react-router-dom";
interface IProps {
  history: any;
  user: IProfile | any;
}
interface IProfile {
  student_id: number | any;
  role: string | any;
  fname: string | any;
  mname: string | any;
  lname: string | any;
  email: string | any;
  phone_number: string | any;
  employment_status: string | any;
  job_opportunities: string | any;
  job_title: string | any;
  objectives: string | any;
  institute_names_array: string[] | any;
  institute_degree_names_array: string[] | any;
  institute_locations_array: string[] | any;
  field_of_studies_array: string[] | any;
  study_intervals_array: string[] | any;
  gpa_score: string[] | any;
  company_names_array: string[] | any;
  company_locations_array: string[] | any;
  job_titles_array: string[] | any;
  job_descriptions_array: string[] | any;
  work_intervals_array: string[] | any;
  project_titles_array: string[] | any;
  project_descriptions_array: string[] | any;
  skills_array: string[] | any;
  skill_levels_array: number[] | any;
  area_of_interests: string | any;
}

export default class NavBar extends React.Component<IProps> {
  handleLogout = () => {
    localStorage.clear();
    this.props.history.push("/login");
  };

  user = () => {
    return !this.props.user
      ? this.props.history.location.state.user
      : this.props.user;
  };

  myprofileLink = () => {
    return this.props.history.location.pathname === "/myprofile" ? null : (
      <p className="myprofile">
        <Link className="has-text-primary" to="/myprofile">
          Back to my profile
        </Link>
      </p>
    );
  };

  render() {
    return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="container">
          <div className="navbar-start">
            <p className="navbar-item">
              {/* <img
                className="profile-pic"
                src={`https://www.gravatar.com/avatar/${md5(
                  this.user().email
                )}`}
                alt="User profile pic"
              /> */}
              {this.user().fname && this.user().lname ? (
                <p> Hello {this.user().fname + " " + this.user().lname}</p>
              ) : null}
            </p>
          </div>
          <div className="navbar-center">{this.myprofileLink()}</div>
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                {/* <button
                  className="button is-primary has-text-weight-bold"
                  onClick={this.handleNewPost}
                >
                  New Post
                </button> */}

                <button
                  className="button is-primary has-text-weight-bold"
                  onClick={() => this.props.history.push("/vacancies")}
                >
                  Vacancies
                </button>

                <button
                  className="button is-primary has-text-weight-bold"
                  onClick={this.handleLogout}
                >
                  Log out
                </button>
              </div>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}
